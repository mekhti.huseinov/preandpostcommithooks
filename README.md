# PreAndPostCommitHooks
Lets consider we have a dockerized .net project and a Zscaler on a 
local development machine. The Dockerfile looks like this:
```
FROM mcr.microsoft.com/dotnet/core/sdk:3.0.100-preview9 AS builder

WORKDIR /src
COPY src/DotNetConf2019.csproj .

ADD src/ca.crt  /usr/local/share/ca-certificates/ca.crt
RUN chmod 644 /usr/local/share/ca-certificates/ca.crt  && update-ca-certificates

RUN dotnet restore

COPY src/ .
RUN dotnet publish -c Release -o /out DotNetConf2019.csproj

# app image
FROM mcr.microsoft.com/dotnet/core/runtime:3.0.0-preview9

WORKDIR /app
ENTRYPOINT ["dotnet", "DotNetConf2019.dll"]
ENV DotNetBot:Message="docker4theEdge!"

COPY --from=builder /out/ .


```
The main problem of this configuration is in lines with 'ca.crt' which is 
Zscaler certificate and we do not want it to appear in clients repository. 
So in order to automatically prevent this configuration to be commited to the 
repository and at the same time keep our configuration for local development 
we can use pre-commit and post-commit hooks in git:

*  pre-commit will remove specific lines from Dockerfile
*  post-commit will bring them back.

Hooks should be added into the folder /.git/hooks 
pre-commit:
```
#!/bin/sh

sed -i '/ca.crt/d' ./webRequests/Dockerfile
git add ./webRequests/Dockerfile
```
post-commit:
```
#!/bin/sh

sed -i '13 i ADD src/ca.crt  /usr/local/share/ca-certificates/ca.crt' ./webRequests/Dockerfile
sed -i '14 i RUN chmod 644 /usr/local/share/ca-certificates/ca.crt  && update-ca-certificates' ./webRequests/Dockerfile
```

https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks